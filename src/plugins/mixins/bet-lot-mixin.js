import { sumBy, filter } from "lodash";
import EventBus from "../configs/event-bus";

const betLotMixin = {
  props: ["gameData", "disallowBetting", "minutesString", "secondsString"],
  data() {
    return {
      stakeRecords: []
    };
  },
  mounted() {
    EventBus.$on("update-session-storage", this.onUpdateSessionStorage);
  },
  methods: {
    odd(type) {
      return this.gameData.odds && this.gameData.odds[type];
    },
    onUpdateSessionStorage() {
      this.stakeRecords = JSON.parse(sessionStorage.getItem("stake-records"));
    },
    stakeRecordSum(type) {
      let filteredRecords = filter(
        this.stakeRecords,
        item => parseInt(item.gID) === this.gameData.gID && item.type === type
      );
      return sumBy(filteredRecords, "stake");
    }
  }
};

export default betLotMixin;
