import Domino from "./components/domino/Domino";
import Baccarat from "./components/baccarat/Baccarat";

import "iview/dist/styles/iview.css";
import "./assets/styles/less/index.less";

const Max3D = {};

Max3D.install = function(Vue) {
  Vue.component("domino", Domino);
  Vue.component("baccarat", Baccarat);
};

export default Max3D;
