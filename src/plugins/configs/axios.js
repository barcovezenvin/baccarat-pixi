import axios from "axios";

export const baseURL = () => {
  if (process.env.NODE_ENV === "development") {
    return "http://13.229.14.229/dummy/";
  }
};

axios.interceptors.request.use(
  function(config) {
    // Replace the current config for backend url
    config = {
      ...config
      // url: config.url.replace(/(\/data|\&timestamp\=\d+|ll\=\d+)/gi, "")
    };

    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

axios.defaults.baseURL = baseURL();

export default axios;
