/***************
 * Action types
 ***************/
const betlotInitialState = () => {
  return {
    isShowingResult: false,
    stake: "",
    rules: null,
    betting: false,
    // responseMessage: null,
    refNo: "",
    setStakeTimeout: null,
    closeResponsePopperTimeout: null,
    dice1SelectedType: "o11",
    dice2SelectedType: "o21",
    dice3SelectedType: "o31",
    dice4SelectedType: "o41",
    dice5SelectedType: "o51",
    dice6SelectedType: "o61"
  };
};

const openWindowSize = [800, 600];

// Constant

const baccarat_betTypes = {
  o1: { code: "o1", betID: 1, gameTypeID: 1 },
  o2: { code: "o2", betID: 2, gameTypeID: 1 },
  o3: { code: "o3", betID: 3, gameTypeID: 1 },
  o4: { code: "o4", betID: 4, gameTypeID: 2 },
  o5: { code: "o5", betID: 5, gameTypeID: 2 },
  o6: { code: "o6", betID: 6, gameTypeID: 2 },
  o7: { code: "o7", betID: 7, gameTypeID: 2 },
  o8: { code: "o8", betID: 8, gameTypeID: 3 },
  o9: { code: "o9", betID: 9, gameTypeID: 3 },
  o10: { code: "o10", betID: 10, gameTypeID: 4 }
};

const domino_betTypes = {
  o1: { code: "o1", betID: 1, gameTypeID: 1 },
  o2: { code: "o2", betID: 2, gameTypeID: 1 },
  o3: { code: "o3", betID: 3, gameTypeID: 1 },
  o4: { code: "o4", betID: 4, gameTypeID: 2 },
  o5: { code: "o5", betID: 5, gameTypeID: 2 },
  o6: { code: "o6", betID: 6, gameTypeID: 3 },
  o7: { code: "o7", betID: 7, gameTypeID: 3 },
  o8: { code: "o8", betID: 8, gameTypeID: 4 },
  o9: { code: "o9", betID: 9, gameTypeID: 4 },
  o10: { code: "o10", betID: 10, gameTypeID: 5 },
  o11: { code: "o11", betID: 11, gameTypeID: 5 }
};

const languages = [
  {
    name: "English",
    code: "en"
  },
  {
    name: "\u4e2d\u6587",
    code: "zh"
  },
  {
    name: "Bahasa Indonesia",
    code: "id"
  },
  {
    name: "Ti\u1ebfng Vi\u1ec7t",
    code: "vi"
  },
  {
    name: "\u0e44\u0e17\u0e22",
    code: "th"
  },
  {
    name: "\u65e5\u672c\u8a9e",
    code: "ja"
  },
  {
    name: "\ud55c\uad6d\uc5b4",
    code: "ko"
  },
  {
    name: "Burmese",
    code: "my"
  }
];

export {
  betlotInitialState,
  // placeBetUrl,
  // replaceBetUrl,
  // apiUrl,
  // customerUrl,
  // customerBalanceUrl,
  // drawsCheckUpdateUrl,
  // drawResultURL,
  // nextDrawsUrl,
  // checkbetTypeUrl,
  // RebetUrl,
  // trendlotUrl,
  // custBetUrl,
  // langUrl,
  // logoutUrl,
  // forceLogoutUrl,
  // betsUrl,
  // statementUrl,
  // gamehistoryUrl,
  // rulesUrl,
  // notificationUrl,
  openWindowSize,
  languages,
  domino_betTypes,
  baccarat_betTypes
};
