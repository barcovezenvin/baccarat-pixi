// Localization file, segregated based on game name.
// *** NOTE ***
// Those general info are used across different games
// should always maintained in general object to prevent
// double info. Please refer en.js as full example.

const id = {
  general: {
    wallet: "Kredit Taruhan",
    menulogout: "Logout",
    goodroad: "Dragon Alert",
    menufavourite: "Favourite",
    menusfx: "SFX",
    menuon: "ON",
    menuoff: "OFF",
    menurules: "Peraturan",
    menuresult: "Hasil",
    menustatement: "Laporan",
    menunotification: "Pengumuman",
    menuviewhistory: "Lihat Riwayat",
    menufullscreen: "Layar Penuh",
    marketclosed: "Pasar Tutup",
    menutrend: "Tren",
    menusimple: "Sederhana",
    menunormal: "Normal",

    draw_no: "Ronde No.",
    rebet: "Taruhan Ulang",
    no_more_bet: "Tidak Menerima Taruhan Lagi",
    sum: "JUMLAH",
    next_draw: "Penarikan Berikutnya",
    last_bets_for: "Taruhan terakhir untuk",
    bet_total: "Total Taruhan",
    no_bets_last_round: "Tidak ada taruhan pada ronde terakhir.",
    max: "Max",
    payout: "Pembayaran",
    min_to: "Minimum",
    max_to: "Maksimum",
    cancel: "Batal",
    bet: "Pasang Taruhan",
    ref: "Ref. No",
    gap: "CELAH",

    sum_bs_short: "JUMLAH B/k",
    sum_oe_short: "JUMLAH Ga/Ge",
    Big: "Besar",
    Small: "Kecil",
    Even: "Genap",
    Odd: "Ganjil",
    Dice: "Dadu",
    Triple: "Kembar Tiga",
    High: "Tinggi",
    Low: "Rendah",
    Black: "Hitam",
    Red: "Merah",
    Joker: "Pelawak",

    //for history TabPane
    Result: "Hasil",
    Sum_Player: "Pemain Jumlah",
    Sum_Banker: "Bankir Jumlah",
    CARD_HISTORY: "SEJARAH KARTU",
    STATISTICS: "STATISTIK",
    RED_BLACK: "MERAH/HITAM",
    HI_LO: "NAIK/TURUN",
    Player: "Pemain",
    Tie: "Seri",
    Banker: "Bankir",
    PLACE_TICKET: "Taruhan",
    PLACE_FAIL: "Taruhan Tidak Berhasil!",
    PLACE_SUCCESS: "Taruhan Berhasil!",
    ERROR_MIN_TO: "Maaf, modal tidak boleh kurang dari minimum taruhan!",
    ERROR_MAX_TO: "Maaf, modal tidak boleh lebih dari maksimum taruhan!",
    ERROR_MAX_DR: "Maaf, Anda telah melebihi Maksimum Taruhan Per Draw!",
    ERROR_MAX_BTDR:
      "Maaf, jenis taruhan Anda telah melebihi Maksimum Taruhan Per Draw!",
    ERROR_NMB: "Maaf, tidak menerima taruhan lagi.",
    ERROR_GAME_UA:
      "Taruhan sedang tidak tersedia. Silakan hubungi kami untuk rinciannya, terima kasih.",
    ERROR_REBET_DONE: "Maaf, Anda sudah rebet taruhan sebelumnya.",
    ERROR_TO: "Maaf, mohon untuk memasukkan modal taruhan Anda kembali!",
    ERROR_XAMOUNT: "Maaf, sisa saldo Anda tidak cukup untuk memasang taruhan!",
    ERROR_206:
      "Anda telah mencapai limit kemenangan! Silakan hubungi pihak Asosiasi Anda, terima kasih.",
    ERROR_207:
      "Anda telah mencapai limit kekalahan Silakan hubungi pihak Asosiasi Anda, terima kasih.",
    ERROR_208:
      "Akun ditangguhkan! Silakan hubungi pihak Asosiasi Anda, terima kasih.",
    ERROR_209:
      "Akun tidak aktif! Silakan hubungi pihak Asosiasi Anda, terima kasih."
  },

  max_dice: {
    dicehistory: "DICE HISTORY",
    o1group: "Besar/Kembar Tiga/Kecil",
    o2group: "Besar/Kembar Tiga/Kecil",
    o3group: "Besar/Kembar Tiga/Kecil",
    o4group: "Ganjil/Genap",
    o5group: "Ganjil/Genap",

    o11group: "Nomor tertentu",
    o21group: "Nomor tertentu",
    o31group: "Nomor tertentu",
    o41group: "Nomor tertentu",
    o51group: "Nomor tertentu",
    o61group: "Nomor tertentu",
    o104group: "Total spesifik",
    o105group: "Total spesifik",
    o106group: "Total spesifik",
    o107group: "Total spesifik",
    o108group: "Total spesifik",
    o109group: "Total spesifik",
    o110group: "Total spesifik",
    o111group: "Total spesifik",
    o112group: "Total spesifik",
    o113group: "Total spesifik",
    o114group: "Total spesifik",
    o115group: "Total spesifik",
    o116group: "Total spesifik",
    o117group: "Total spesifik",

    o1: "Besar",
    o1_trend: "B",
    o2: "Kecil",
    o2_trend: "K",
    o3: "Kembar Tiga",
    o3_trend: "T",
    o4: "Ganjil",
    o4_trend: "Ga",
    o5: "Genap",
    o5_trend: "Ge",
    o6: "4",
    o6_trend: "4",
    o7: "5",
    o7_trend: "5",
    o8: "6",
    o8_trend: "6",
    o9: "7",
    o9_trend: "7",
    o10: "8",
    o10_trend: "8",
    o11: "9",
    o11_trend: "9",
    o12: "10",
    o12_trend: "10",
    o13: "11",
    o13_trend: "11",
    o14: "12",
    o14_trend: "12",
    o15: "13",
    o15_trend: "13",
    o16: "14",
    o16_trend: "14",
    o17: "15",
    o17_trend: "15",
    o18: "16",
    o18_trend: "16",
    o19: "17",
    o19_trend: "17",

    o11: "Dadu 1",
    o12: "2x Dadu 1",
    o13: "3x Dadu 1",
    o21: "Dadu 2",
    o22: "2x Dadu 2",
    o23: "3x Dadu 2",
    o31: "Dadu 3",
    o32: "2x Dadu 3",
    o33: "3x Dadu 3",
    o41: "Dadu 4",
    o42: "2x Dadu 4",
    o43: "3x Dadu 4",
    o51: "Dadu 5",
    o52: "2x Dadu 5",
    o53: "3x Dadu 5",
    o61: "Dadu 6",
    o62: "2x Dadu 6",
    o63: "3x Dadu 6",

    o104: "4",
    o104_trend: "4",
    o105: "5",
    o105_trend: "5",
    o106: "6",
    o106_trend: "6",
    o107: "7",
    o107_trend: "7",
    o108: "8",
    o108_trend: "8",
    o109: "9",
    o109_trend: "9",
    o110: "10",
    o110_trend: "10",
    o111: "11",
    o111_trend: "11",
    o112: "12",
    o112_trend: "12",
    o113: "13",
    o113_trend: "13",
    o114: "14",
    o114_trend: "14",
    o115: "15",
    o115_trend: "15",
    o116: "16",
    o116_trend: "16",
    o117: "17",
    o117_trend: "17",

    "1_dice": "1 Dadu",
    "2_dice": "2 Dadu",
    "3_dice": "3 Dadu"
  },

  hi_lo: {
    o1: "Naik",
    o1_trend: "N",
    o2: "Turun",
    o2_trend: "T",
    o3: "Merah",
    o3_trend: "M",
    o4: "Hitam",
    o4_trend: "H",
    o5: "2-9",
    o5_trend: "2-9",
    o6: "JQKA",
    o6_trend: "JQKA",
    o7: "KA",
    o7_trend: "KA",
    o101: "A",
    o101_trend: "A",
    o102: "2",
    o102_trend: "2",
    o103: "3",
    o103_trend: "3",
    o104: "4",
    o104_trend: "4",
    o105: "5",
    o105_trend: "5",
    o106: "6",
    o106_trend: "6",
    o107: "7",
    o107_trend: "7",
    o108: "8",
    o108_trend: "8",
    o109: "9",
    o109_trend: "9",
    o110: "J",
    o110_trend: "J",
    o111: "Q",
    o111_trend: "Q",
    o112: "K",
    o112_trend: "K",
    o113: "Pelawak",
    o113_trend: "P",

    //for new-bet-lot
    o1group: "Naik/Turun",
    o2group: "Naik/Turun",
    o3group: "Merah/Hitam",
    o4group: "Merah/Hitam",
    o5group: "2-9",
    o6group: "JQKA",
    o7group: "KA",
    o101group: "A",
    o102group: "2",
    o103group: "3",
    o104group: "4",
    o105group: "5",
    o106group: "6",
    o107group: "7",
    o108group: "8",
    o109group: "9",
    o110group: "J",
    o111group: "Q",
    o112group: "K",
    o113group: "Pelawak"
  },

  domino: {
    result: "MAIN",
    main: "Main",
    o1: "Pemain",
    o1_trend: "P",
    o2: "Seri",
    o2_trend: "S",
    o3: "Bankir",
    o3_trend: "B",
    o4: "Besar",
    o4_trend: "B",
    o5: "Kecil",
    o5_trend: "K",
    o6: "Ganjil",
    o6_trend: "Ga",
    o7: "Genap",
    o7_trend: "Ge",
    o8: "Besar",
    o8_trend: "B",
    o9: "Kecil",
    o9_trend: "K",
    o10: "Ganjil",
    o10_trend: "Ga",
    o11: "Genap",
    o11_trend: "Ge",
    o12_trend: "/",

    //for new-bet-lot
    o1group: "Pemain/Seri/Bankir",
    o2group: "Pemain/Seri/Bankir",
    o3group: "Pemain/Seri/Bankir",
    o4group: "Pemain Besar/Kecil",
    o5group: "Pemain Besar/Kecil",
    o6group: "Pemain Ganjil/Genap",
    o7group: "Pemain Ganjil/Genap",
    o8group: "Bankir Besar/Kecil",
    o9group: "Bankir Besar/Kecil",
    o10group: "Bankir Ganjil/Genap",
    o11group: "Bankir Ganjil/Genap",

    P_BS: "P B/K",
    P_OE: "P Ga/Ge",
    B_BS: "B B/K",
    B_OE: "B Ga/Ge",

    PLAYER: "PEMAIN",
    BANKER: "BANKIR",
    PLAYER_WIN: "PEMAIN MENANG",
    BANKER_WIN: "BANKIR MENANG",
    TIE: "SERI",

    PLAYER_BIG: "PEMAIN BESAR",
    PLAYER_SMALL: "PEMAIN KECIL",
    PLAYER_ODD: "PEMAIN GANJIL",
    PLAYER_EVEN: "PEMAIN GENAP",

    BANKER_BIG: "BANKIR BESAR",
    BANKER_SMALL: "BANKIR KECIL",
    BANKER_ODD: "BANKIR GANJIL",
    BANKER_EVEN: "BANKIR GENAP"
  },
  maxracing: {
    o1: "C(id)",
    o1_trend: "C(id)",
    o2: "Big(id)",
    o2_trend: "B(id)",
    o3: "Small(id)",
    o3_trend: "S(id)",
    o4: "Odd(id)",
    o4_trend: "O(id)",
    o5: "Even(id)",
    o5_trend: "E(id)",
    o6: "Big(id)",
    o6_trend: "B(id)",
    o7: "Small(id)",
    o7_trend: "S(id)",
    o8: "Odd(id)",
    o8_trend: "O(id)",
    o9: "Even(id)",
    o9_trend: "E(id)",
    o10: "G1(id)",
    o10_trend: "G1(id)",
    o11: "G2(id)",
    o11_trend: "G2(id)",
    o12: "G3(id)",
    o12_trend: "G3(id)",
    o13: "G4(id)",
    o13_trend: "G5(id)",
    o14: "G5(id)",
    o14_trend: "G5(id)",

    //for new-bet-lot
    o1group: "Player/Tie/Banker",
    o2group: "Player/Tie/Banker",
    o3group: "Player/Tie/Banker",
    o4group: "Player Big/Small",
    o5group: "Player Big/Small",
    o6group: "Player Odd/Even",
    o7group: "Player Odd/Even",
    o8group: "Banker Big/Small",
    o9group: "Banker Big/Small",
    o10group: "Banker Odd/Even",
    o11group: "Banker Odd/Even",

    Champion: "Champion(id)",
    C_BS: "C B/S(id)",
    C_OE: "C O/E(id)",
    S_BS: "S B/S(id)",
    S_OE: "S O/E(id)",
    Group: "Group(id)",
    Place_champ: "Place Champion(id)",
    Place_sum_champ: "Place Sum Champion(id)",
    Runner_up: "Runner-up(id)",
    Race_result: "Race Result(id)"
  }
};

export default id;
